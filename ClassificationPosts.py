import pandas as pd
import numpy as np
import nltk
import os.path
import joblib

from ClassCleaner import TextCleaner
from collections import Counter
from sklearn.model_selection import cross_val_score
from Utils import Utils


posts = pd.read_csv('PostSorter.csv')

titleQuestionText = posts['Title']
questionText = posts['Body'].str.lower()

answerText = posts['AnswerAccepted']


cleaner = TextCleaner()

titleTextClean = [cleaner.filter(title) for title in titleQuestionText]
questionTextClean = [cleaner.filter(question) for question in questionText]
answerTextClean = [cleaner.filter(answer) for answer in answerText]

utils = Utils()

def TokenizeAndVectorize(texts):
    
    #nltk.download('punkt') #tokenize
    textsTokenize = [nltk.tokenize.word_tokenize(text) for text in texts]
    stemmer = nltk.stem.snowball.SnowballStemmer('english')
    
    try:
        translate = joblib.load('translateDictionary.pkl')
    except:
        #stopwords from english
        stopwords = nltk.corpus.stopwords.words('english')
        #unique values
        dictionary = set()

        for text in textsTokenize:
            isValidWord = [stemmer.stem(word) for word in text if word not in stopwords and len(word) > 3]
            dictionary.update(isValidWord)

        wordsCount = len(dictionary)

        #added number in each word and after generates an dict
        translate = dict(zip(dictionary, range(wordsCount)))
        joblib.dump(translate, 'translateDictionary.pkl')
    #vectorize and fills a vector with the number of occurrences of word

    def vectorize_text(text, translate):
        vector = [0]*len(translate)
        for word in text:
            if len(word) > 0:
                root = stemmer.stem(word)
                if root in translate:
                    position = translate[root]
                    vector[position] += 1
        return vector

    #vectorize all
    textVectorizes = [vectorize_text(text, translate) for text in textsTokenize]
    return np.array(textVectorizes)

def defineBestModel(X, Y):
        
    #distributes the data for training and validation
    trainingPercent = 0.8

    sizeOfTraining = trainingPercent * len(Y)
    sizeOfValidation = len(Y) - sizeOfTraining

    #first 80% of data
    dataTraining = X[0:int(sizeOfTraining)]
    #first 80% of markings
    marksTraining = Y[0:int(sizeOfTraining)]

    #last's 20% for training "in real life"
    dataValidation = X[int(sizeOfTraining):]
    marksValidation = Y[int(sizeOfTraining):]

    def trainingAndPredict(name, model, dataTraining, marksTraining):
        #needed k-fold
        #break in into three pieces
        k = 3
        scores = cross_val_score(model, dataTraining, marksTraining, cv = k)
        hitRate = np.mean(scores)

        msg = "Hite rate {0}: {1}".format(name, hitRate)
        print(msg)
        return hitRate

    results = {}

    from sklearn.multiclass import OneVsRestClassifier

    #needed e.
    from sklearn.svm import LinearSVC
    #ramdom_state = 0 -> not randomic
    modelOneVsRest = OneVsRestClassifier(LinearSVC(random_state = 0))
    resultOneVsRest = trainingAndPredict("OneVsRestClassifier", modelOneVsRest, dataTraining, marksTraining)
    #added into dictionary
    results[resultOneVsRest] = modelOneVsRest

    from sklearn.multiclass import OneVsOneClassifier
    modelOneVsOne = OneVsOneClassifier(LinearSVC(random_state = 0))
    resultOneVSOne = trainingAndPredict("OneVsOne", modelOneVsOne, dataTraining, marksTraining)
    results[resultOneVSOne] = modelOneVsOne

    from sklearn.naive_bayes import MultinomialNB
    modelMultinomialNB = MultinomialNB()
    resultMultinomial = trainingAndPredict("MultinomialNB", modelMultinomialNB, dataTraining, marksTraining)
    results[resultMultinomial] = modelMultinomialNB

    from sklearn.ensemble import AdaBoostClassifier
    modelAdaBoost = AdaBoostClassifier()
    resultAdaBoost = trainingAndPredict("AdaBoostClassifier", modelAdaBoost, dataTraining, marksTraining)
    results[resultAdaBoost] = modelAdaBoost

    #check the best algorithm
    maxResult = max(results)
    #save winning model
    winner = results[maxResult]
    print("Winner: ")
    print(winner)
    winner.fit(dataTraining, marksTraining)
    
    resultWinner = winner.predict(dataValidation)

    hits = (resultWinner == marksValidation)

    # sum all trues.
    #Note: In python True is equals 1
    totalHits = sum(hits)

    totalElements = len(marksValidation)

    hitRate = (totalHits/totalElements)*100.0

    msg = "Winner total hits rate in real life: {0}".format(hitRate)
    print(msg)

    #algorithm kicks all
    baseHit = max(Counter(marksValidation).values())
    baseHitRate = (baseHit/len(marksValidation))*100
    print("Rate 'dumb': %.2f" %(baseHitRate))
    print("Total validated data: %d" %(len(marksValidation)))

    return winner

def saveModelWinner(model):
    try:
        print("Saving...")
        joblib.dump(model, 'modelWinner.pkl')
        print("Ok, been successfully saved!")
    except IOError:
        print("Sorry, could not save file!")
        print("Trying again! ...")
        saveModelWinner(model)
    
#manualSorter for classification manual
#marks
isRefactoring = utils.manual_classification('persistent_classifier.pkl',questionTextClean)


#vectorize all
questionVectorizes = TokenizeAndVectorize(questionTextClean)

X = questionVectorizes
Y = isRefactoring


try:
    print("***loading model***")
    modelWinner = joblib.load('modelWinner.pkl')
except:
    modelWinner = defineBestModel(X,Y)
    saveModelWinner(modelWinner)

newPosts = pd.read_csv('newPosts.csv')

newQuestionText = newPosts['Body'].str.lower()

newQuestionTextClean = [cleaner.filter(question) for question in newQuestionText]

newQuestionVectorizes = TokenizeAndVectorize(newQuestionTextClean)

newPostsPredict = modelWinner.predict(newQuestionVectorizes)

dataFrame = pd.DataFrame(newQuestionTextClean, index = newPostsPredict)

print(dataFrame.ix[1])



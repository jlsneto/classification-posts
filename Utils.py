'''
Utils v.02
Author: github.com/jlsneto
'''

import joblib
import re


class Utils:

    def __init__(self):
        pass

    def manual_classification(self, path, documment):

        def set_answer(text):
            print(text)
            try:
                answer = input("Set value [0-9]: ")
                print('\n\n')
                match = re.search(r'^[-+]?[0-9]+$','1') #is number
                if match:
                    return answer
                else:
                    raise ValueError
            except ValueError:
                print("Ops, that was no valid value. Please, try again!")
                print("\n")
                return set_answer(text)

        answers = self.load_job(path)
        total_texts = len(documment)
        #Nice
        init = len(answers)
        for text in documment[init:]:
            answers.append(set_answer(text))
            msg = "{0} documments left to classify".format(total_texts-len(answers))
            print(msg)
            print("\n")
            self.save_job(path, answers)
        return answers

    def define_top_words(self, path, top_words):
        words_loaded = self.load_job(path)
        for word in top_words:
            word = self.valid_word(word)
            if word not in words_loaded:
                words_loaded.append(word)
        self.save_job(path, words_loaded)
       

    def valid_word(self, word):
        try:
            match = re.search(r'[\W]|[0-9]', word)  # if matching, then not is valid
            if not match:
                return word
            else:
                raise ValueError
        except ValueError:
            print("Ops! the word %s not is valid!" % (word))
            word = input("Try valid word: ")
            return self.valid_word(word)

    def load_job(self, path):
        try:
            print("Processing file...")
            job = joblib.load(path)
            print("ok!")
            return job
        except IOError:
            print("Sorry, file not found!")
            return []

    def save_job(self, path, job):
        try:
            print("Saving...")
            joblib.dump(job, path)
        except IOError:
            print("Sorry, could not save file!")
            print("Trying again! ...")
            self.save_job(job)

if __name__ == "__main__":
    cf = Utils()

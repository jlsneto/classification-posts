import pandas as pd
import nltk
import random

from ClassCleaner import TextCleaner
from Utils import Utils

posts = pd.read_csv('PostSorter.csv')


titleQuestionText = posts['Title']
questionText = posts['Body'].str.lower() #using body



def clean_texts(text_list):
    
    cleaner = TextCleaner()
    questionTextClean = [cleaner.filter(question) for question in text_list]

    return questionTextClean

def tokenize_texts(text_list):
    
    textsTokenize = [nltk.tokenize.wordpunct_tokenize(text) for text in text_list]

    #[textsTokenize.append(answer) for answer in answerTextTokenize]
    #isRefactoring = isRefactoring + isRefactoring

    return textsTokenize


questionTextClean = clean_texts(questionText)
textsTokenize = tokenize_texts(questionTextClean)

#Manual Classify load job
utils = Utils()
isRefactoring = utils.manual_classification("persistent_classifier.pkl",questionTextClean)




documents = [(textsTokenize[i],isRefactoring[i]) for i in range(len(isRefactoring))]

#if not shuffle the predict is 70%
random.shuffle(documents)

stopWords = nltk.corpus.stopwords.words('english')


#top_words is manually defined words
top_words = utils.load_job('stop_words.pkl')

#if the word size is between 9 and 10 the prediction increases
all_words = nltk.FreqDist(word for text in textsTokenize
                          for word in text
                              if word not in stopWords and len(word) in range(9,10))
#all_words = all_words.most_common(1000)
word_features = utils.load_job('last_word_features.pkl')

def document_features(document):
    document_words = set(document)
    features = {}
    for word in word_features:
        features['contains({})'.format(word)] = (word in document_words)
    return features



featuresets = [(document_features(d), c) for (d,c) in documents]
#classifier.classify(featuresets[0][0])

train_set, test_set = featuresets[:70], featuresets[70:]

classifier = nltk.NaiveBayesClassifier.train(train_set)

accuracy = nltk.classify.accuracy(classifier,test_set)
print("Accuracy:")
print(accuracy)

#save
#utils.save_job("last_train_classifier.pkl",classifier)

#model compare
best_model = utils.load_job('best_model.pkl')
best_model_accuracy = nltk.classify.accuracy(best_model,test_set)

if accuracy > best_model_accuracy:
    utils.save_job("last_word_features.pkl",word_features)  
    utils.save_job('best_model.pkl',classifier)

def word_of_predicts(featuresets):
    yn = int(input("1 or 0: ")) 
    words = []
    for i in range(len(featuresets)):
        if (featuresets[i][1] == False): 
            for j in featuresets[i][0]:
                if(featuresets[i][0][j] == True):
                    if j not in words:
                        words.append(j)
    return words



postsValid = pd.read_csv('newPosts.csv')
validQuestionText = postsValid['Body'].str.lower()

validQuestionTextClean = clean_texts(validQuestionText)
textsTokenizeValid = tokenize_texts(validQuestionTextClean)


newFeatureSets = [(document_features(d)) for d in textsTokenizeValid]

newPredict = [best_model.classify(feature) for feature in newFeatureSets]

dataFrame = pd.DataFrame(validQuestionTextClean, index = newPredict)
print(dataFrame.ix[1])

classification = [i[0] for i in dataFrame.ix[1].values.tolist()]

clusters = utils.manual_classification("clusters.pkl",classification)

classificationTokenize = tokenize_texts(classification)

classificationDocumments = [(classificationTokenize[i],clusters[i]) for i in range(len(clusters))]

random.shuffle(classificationDocumments)

all_words = nltk.FreqDist(word for text in classificationTokenize
                          for word in text
                              if word not in stopWords and len(word) in range(9,10))

featuresets = [(document_features(d), c) for (d,c) in classificationDocumments]
#classifier.classify(featuresets[0][0])
index = round(len(featuresets)*0.7)

train_set, test_set = featuresets[:index], featuresets[len(featuresets)-index:]

classifier = nltk.NaiveBayesClassifier.train(train_set)

accuracy = nltk.classify.accuracy(classifier,test_set)
print("Accuracy:")
print(accuracy)
